#include "coeur.h"
#include "coeur.c"
#include "param.h"
#include <Arduino.h>

int pinLed[10] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 13};              //déclaration et initialisation du tableau

void setup(){
setup_coeur(pinLed);
}

void loop(){
coeur(pinLed);   
}
